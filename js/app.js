//webkitURL is deprecated but nevertheless
URL = window.URL || window.webkitURL;



var isEdge = navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator
	.msSaveBlob);
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
var mimeType = 'video/webm';
var type = 'video';
var fileExtension = "mp4";
var recorder; // globally accessible
var camera;
var microphone;
var counter = 1;
var iii = 0;
var is_recording = 1;
var blobs;


var video = document.querySelector('video');
var audio = document.querySelector('audio');

var recoding_marker = document.getElementById('recoding_marker');





var gumStream; //stream from getUserMedia()
var input; //MediaStreamAudioSourceNode  we'll be recording
var encodingType; //holds selected encoding for resulting audio (file)
var encodeAfterRecord = true; // when to encode

// shim for AudioContext when it's not avb. 
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext; //new audio context to help us record


function calculateTimeDuration(secs) {
	var hr = Math.floor(secs / 3600);
	var min = Math.floor((secs - (hr * 3600)) / 60);
	var sec = Math.floor(secs - (hr * 3600) - (min * 60));

	if (min < 10) {
		min = "0" + min;
	}

	if (sec < 10) {
		sec = "0" + sec;
	}

	if (hr <= 0) {
		return min + ':' + sec;
	}

	return hr + ':' + min + ':' + sec;
}

function startRecording() {
	console.log("startRecording() called");

	var constraints = {
		audio: true,
		video: false
	}

	navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {

		audioContext = new AudioContext();

		gumStream = stream;

		input = audioContext.createMediaStreamSource(stream);

		encodingType = "mp3";

		recorder = new WebAudioRecorder(input, {
			workerDir: "js/", // must end with slash
			encoding: encodingType,
			numChannels: 2, //2 is the default, mp3 encoding supports only 2
			onEncoderLoading: function (recorder, encoding) {

			},
			onEncoderLoaded: function (recorder, encoding) {

			}
		});

		recorder.onComplete = function (recorder, blob) {
			createDownloadLink(blob, recorder.encoding);

		}

		recorder.setOptions({
			timeLimit: 120,
			encodeAfterRecord: encodeAfterRecord,
			ogg: {
				quality: 0.5
			},
			mp3: {
				bitRate: 160
			}
		});

		//start the recording process
		recorder.startRecording();
		setInterval(function () {
				if (is_recording == 1) {
					iii++;
					document.querySelector('h1').innerHTML = calculateTimeDuration(iii);
				} else return;

			},
			1000);

	}).catch(function (err) {


	});

}

function stopRecording() {
	console.log("stopRecording() called");
	is_recording = 0;
	document.querySelector('h1').innerHTML = "";

	//stop microphone access
	gumStream.getAudioTracks()[0].stop();

	//disable the stop button
	audio.muted = false;
	audio.volume = 1;

	//tell the recorder to finish the recording (stop recording + encode the recorded audio)
	recorder.finishRecording();

}

function createDownloadLink(blob, encoding) {

	var url = URL.createObjectURL(blob);
	audio.controls = true;
	audio.src = url;
	blobs = blob;

}


//_______________________________________________________________________________________________________




(function () {
	var params = {},
		r = /([^&=]+)=?([^&]*)/g;

	function d(s) {
		return decodeURIComponent(s.replace(/\+/g, ' '));
	}

	var match, search = window.location.search;
	while (match = r.exec(search.substring(1))) {
		params[d(match[1])] = d(match[2]);

		if (d(match[2]) === 'true' || d(match[2]) === 'false') {
			params[d(match[1])] = d(match[2]) === 'true' ? true : false;
		}
	}

	window.params = params;
})();



if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
	// Not adding `{ audio: true }` since we only want video now
	navigator.mediaDevices.getUserMedia({
			video: true,
			audio: true
		}).then(function (stream) {
			//video.src = window.URL.createObjectURL(stream);
			video.srcObject = stream;
			camera = stream;
			video.play();
		})
		.catch(function (error) {
			alert("没有 Camera");
		});

}

function captureCamera(callback) {
	navigator.mediaDevices.getUserMedia({
		audio: true,
		video: true
	}).then(function (camera) {
		callback(camera);
	}).catch(function (error) {
		alert('Unable to capture your camera. Please check console logs.');
		console.error(error);
	});
}
//______________________________________________________________________________ start recording ________________________
document.getElementById('btn-start-recording').onclick = function () {
	iii = 0;

	if (fileExtension === "mp4") {
		captureCamera(function (camera) {
			video.muted = true;
			video.volume = 0;
			video.srcObject = camera;

			recorder = RecordRTC(camera, {
				type: 'video',
				timeSlice: 1000,

				onTimeStamp: function (timestamp, timestamps) {

					console.log(iii);
					var duration = (new Date().getTime() - timestamps[0]) / 1000;
					if (duration < 0) return;
					document.querySelector('h1').innerHTML = calculateTimeDuration(iii);
					iii++;
				}

			});

			recorder.startRecording();

			recorder.camera = camera;
			recoding_marker.className = "active";
			// release camera on stopRecording
			document.getElementById('btn-start-recording').disabled = true;

		});

	} else {
		
		audio.controls = true;		
		is_recording = 1;
		iii = 0;
		startRecording();

	}

	document.getElementById('btn-stop-recording').disabled = false;
	document.getElementById('btn-pause-recording').disabled = false;

};
//_________________________________________________________________________ stop video ____________
function stopRecordingCallback() {
	document.querySelector('h1').innerHTML = "";

	video.muted = false;
	video.volume = 1;

	video.src = video.srcObject = null;

	getSeekableBlob(recorder.getBlob(), function (seekableBlob) {

		blobs = recorder.getBlob();

		if (fileExtension == "mp4") {
			video.src = URL.createObjectURL(seekableBlob);
			recorder.camera.stop();
			convertStreams(blobs);
		} else {
			audio.src = URL.createObjectURL(seekableBlob);
			audio.play();
			audio.srcObject = null;
		}

		recorder.destroy();
		recorder = null;
		console.log(blobs);
		document.getElementById('btn-start-recording').disabled = false;

		// invokeSaveAsDialog(seekableBlob, getFileName());
	});
}

document.getElementById('btn-stop-recording').onclick = function () {
	this.disabled = true;
	recoding_marker.className = "";
	if (fileExtension === "mp4") {
		// recorder.stopRecording(stopRecordingCallback);
		recorder.stopRecording(function (url) {
			video.src = url;
			video.download = 'video.webm';

			log('<a href="' + workerPath +
				'" download="ffmpeg-asm.js">ffmpeg-asm.js</a> file download started. It is about 18MB in size; please be patient!'
				);
			convertStreams(recorder.getBlob());
		});

	} else {
		stopRecording();
	}
	document.getElementById('btn-start-recording').disabled = false;
	document.getElementById('btn-pause-recording').disabled = true;

};

document.getElementById('btn-pause-recording').onclick = function () {
	this.disabled = true;
	recoding_marker.className = ""

	if (this.innerHTML === '暂 停') {
		recorder.pauseRecording();
		this.innerHTML = '重新开始';
	} else {
		recorder.resumeRecording();
		this.innerHTML = '暂 停';
	}

	setTimeout(function () {
		document.getElementById('btn-pause-recording').disabled = false;
	}, 2000);
};
//____________________________________________________  upload ________________________
function getFileName() {
	var d = new Date();
	var year = d.getUTCFullYear();
	var month = d.getUTCMonth();
	var date = d.getUTCDate();
	return 'Record_' + year + month + date + '-' + d.getTime() + '.' + fileExtension;
}

document.querySelector('#upload-to-php').onclick = function () {
	var fileName = getFileName();
	var file = new File([blobs], getFileName(), {
		type: fileExtension === "mp4" ? 'video/webm' : 'audio/mp3'
	});

	var formData = new FormData();
	formData.append('video-filename', fileName);
	formData.append('video-blob', file);
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			alert("上传 成功")
		}
	};
	xhr.open('POST', 'RecordRTC-to-PHP/save.php', true);
	xhr.send(formData);

};


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++________  Formart Factory Converter   _________________++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

var workerPath = 'https://archive.org/download/ffmpeg_asm/ffmpeg_asm.js';
if (document.domain == 'localhost') {
	workerPath = location.href.replace(location.href.split('/').pop(), '') + 'js/ffmpeg_asm.js';
}

function processInWebWorker() {
	var blob = URL.createObjectURL(
		new Blob(['importScripts("' + workerPath +
			'");var now = Date.now;function print(text) {postMessage({"type" : "stdout","data" : text});};onmessage = function(event) {var message = event.data;if (message.type === "command") {var Module = {print: print,printErr: print,files: message.files || [],arguments: message.arguments || [],TOTAL_MEMORY: message.TOTAL_MEMORY || false};postMessage({"type" : "start","data" : Module.arguments.join(" ")});postMessage({"type" : "stdout","data" : "Received command: " +Module.arguments.join(" ") +((Module.TOTAL_MEMORY) ? ".  Processing with " + Module.TOTAL_MEMORY + " bits." : "")});var time = now();var result = ffmpeg_run(Module);var totalTime = now() - time;postMessage({"type" : "stdout","data" : "Finished processing (took " + totalTime + "ms)"});postMessage({"type" : "done","data" : result,"time" : totalTime});}};postMessage({"type" : "ready"});'
		], {
			type: 'application/javascript'
		}));

	var worker = new Worker(blob);
	URL.revokeObjectURL(blob);
	return worker;
}


var worker;

function convertStreams(videoBlob) {
	var aab;
	var buffersReady;
	var workerReady;
	var posted;

	var fileReader = new FileReader();
	fileReader.onload = function () {
		aab = this.result;
		postMessage();
	};
	fileReader.readAsArrayBuffer(videoBlob);

	if (!worker) {
		worker = processInWebWorker();
	}

	worker.onmessage = function (event) {
		var message = event.data;
		if (message.type == "ready") {
			log('<a href="' + workerPath +
				'" download="ffmpeg-asm.js">ffmpeg-asm.js</a> file has been loaded.');

			workerReady = true;
			if (buffersReady)
				postMessage();
		} else if (message.type == "stdout") {
			log(message.data);
		} else if (message.type == "start") {
			log('<a href="' + workerPath +
				'" download="ffmpeg-asm.js">ffmpeg-asm.js</a> file received ffmpeg command.');
		} else if (message.type == "done") {
			log(JSON.stringify(message));

			var result = message.data[0];
			log(JSON.stringify(result));

			var blob = new File([result.data], 'test.mp4', {
				type: 'video/mp4'
			});

			log(JSON.stringify(blob));

			PostBlob(blob);
		}
	};
	var postMessage = function () {
		posted = true;

		worker.postMessage({
			type: 'command',
			arguments: '-i video.webm -c:v mpeg4 -b:v 6400k -strict experimental output.mp4'
				.split(' '),
			files: [{
				data: new Uint8Array(aab),
				name: 'video.webm'
			}]
		});
	};
}
var inner = document.querySelector('#tmp');
function PostBlob(blob) {
	var video = document.createElement('video');
	video.controls = true;

	var source = document.createElement('source');
	source.src = URL.createObjectURL(blob);
	source.type = 'video/mp4; codecs=mpeg4';
	video.appendChild(source);

	video.download = 'Play mp4 in VLC Player.mp4';

	inner.appendChild(document.createElement('hr'));
	var h2 = document.createElement('h2');
	h2.innerHTML = '<a href="' + source.src +
		'" target="_blank" download="Play mp4 in VLC Player.mp4" style="font-size:200%;color:red;">Download Converted mp4 and play in VLC player!</a>';
	inner.appendChild(h2);
	// h2.style.display = 'block';
	inner.appendChild(video);

	video.tabIndex = 0;
	video.focus();
	video.play();

	
}

var logsPreview = document.getElementById('logs-preview');

function log(message) {
	var li = document.createElement('li');
	li.innerHTML = message;
	logsPreview.appendChild(li);

	li.tabIndex = 0;
	li.focus();
}

window.onbeforeunload = function () {
	document.querySelector('#record-video').disabled = false;
};

//_____________________________________________________________________ swich adio and vido
$(function () {
	$(".recording-media").on("change", function () {
		console.log($(this).val());
		if ($(this).val() == "record-audio-plus-video") {

			video.muted = false;
			$("#id_camera").addClass("active");
			$("#id_mic").removeClass("active");
			document.getElementById('btn-pause-recording').style.display = "";

			if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
				// Not adding `{ audio: true }` since we only want video now
				navigator.mediaDevices.getUserMedia({
						video: true,
						audio: true
					}).then(function (stream) {
						//video.src = window.URL.createObjectURL(stream);
						fileExtension = "mp4";
						video.srcObject = stream;
						camera = stream;
						video.play();
					})
					.catch(function (error) {
						alert("没有 Camera");
					});

			}
		} else {
			$("#id_camera").removeClass("active");
			$("#id_mic").addClass("active");
			video.muted = true;

			if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {

				fileExtension = "mp3";
				navigator.mediaDevices.getUserMedia({
					audio: isEdge ? true : {
						echoCancellation: false
					}
				}).then(function (stream) {
					microphone = stream;
					audio.muted = true;

					document.getElementById('btn-pause-recording').style.display = "none";
					document.getElementById('id_camera').className = "";
					document.getElementById('id_mic').className = "active";

				}).catch(function (error) {
					alert('Unable to capture your microphone.');
					console.error(error);
				});

			}
		}
	});
});

